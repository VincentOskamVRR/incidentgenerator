# -*- coding: utf-8 -*-
"""
Created on Mon Aug 08 08:25:25 2016

@author: oskam
"""



import os
import random
import datetime as dt
import pandas as pd  # data frame operations
import numpy as np  # arrays and math functions
import matplotlib as mpl
import matplotlib.pyplot as plt  # 2D plotting
import seaborn as sns
import random


os.chdir(r'D:\Projecten\2017\rembrand\incidentgenerator')
# pdfs
import v20170109_baselines as pdfs

adres = pd.read_csv("adres.csv",sep=';')

                                                         
print(adres['sted'].value_counts(sort=False))
print(adres.sample(1))

def gen_inc(sted_pdf=[0.0337,0.0225,0.0225,0.0278,0.0426]):
    a,b,c,d,e = sted_pdf
    print(a,b,c,d,e)
    sted1 = adres[adres['sted']==1]['sted'].count()
    n_sted1 = sted1*a
    sted2 = adres[adres['sted']==2]['sted'].count()
    n_sted2 = sted2*b
    sted3 = adres[adres['sted']==3]['sted'].count()
    n_sted3 = sted3*c
    sted4 = adres[adres['sted']==4]['sted'].count()
    n_sted4 = sted4*d
    sted5 = adres[adres['sted']==5]['sted'].count()
    n_sted5 = sted5*e
    #del adres_list
    #adres_list = pd.DataFrame(columns=['object_id', 'x', 'y', 'sted'])
    adres_list = adres[adres['sted']==1].sample(int(n_sted1),replace=True)
    adres_list = adres_list.append(adres[adres['sted']==2].sample(int(n_sted2),replace=True))
    adres_list = adres_list.append(adres[adres['sted']==3].sample(int(n_sted3),replace=True))
    adres_list = adres_list.append(adres[adres['sted']==4].sample(int(n_sted4),replace=True))
    adres_list = adres_list.append(adres[adres['sted']==5].sample(int(n_sted5),replace=True))
    
    # plt.scatter(adres_list['x'],adres_list['y'])
    
    # sns.lmplot('x', 'y', data=adres_list, fit_reg=False, hue="sted", size=10, scatter_kws={"marker": "D", "s": 0.5})
    # plt.title('incidenten')
    
    return adres_list    

cl_inc_pdf = {'Brand: Gebouwbrand' : [0.0028, 0.0017, 0.0023, 0.002, 0.0026]
                  , 'Brand: Buitenbrand' : [0.0041, 0.0030, 0.0025, 0.0044, 0.0054] 
                  , 'Hulpverlening: Assistentie ambulance' : [0.0053, 0.0045, 0.0031, 0.0039, 0.0042] 
                  , 'Overig' : [0.0216, 0.0133, 0.0146, 0.0175, 0.0305]}

    
def gen_inc_cl(cl_inc_pdf=cl_inc_pdf):
    b = pd.DataFrame(columns=['cl','object_id', 'x', 'y', 'sted'])
    for i in cl_inc_pdf:
        print(i)
        print(cl_inc_pdf[i])    
        a = gen_inc(sted_pdf=cl_inc_pdf[i])
        a ['cl'] = i
        b = b.append(a)
    
    if 1==0:
        plt.scatter(b['x'],b['y'])
    
    if 1==1:
        sns.lmplot('x', 'y', data=b, fit_reg=False, hue="cl", size=10, scatter_kws={"marker": "D", "s": 3.5})
        plt.title('incidenten')
    
    return b
        
    """    STED     1       2       3       4       5        
    CL                  
    gebouwbrand     0.28    0.17    0.23    0.2     0.26
    buitenbrand     0.41    0.30    0.25    0.44    0.54
    ass ambu        0.53    0.45    0.31    0.39    0.42
    overig          2.16    1.33    1.46    1.75    3.05    
    """        

    
    
    
    # np.random.choice(range(24), 1, p=pdfs.pdfscluur[i][0])[0]    


def clpdf():
    return 'Brand: Gebouwbrand'

def hourspdf(cl='Brand: Gebouwbrand',weekend=0):    
    return pdfs.pdfscluur[cl][weekend]

def monthpdf(cl='Brand: Gebouwbrand',weekend=0):    
    return pdfs.pdfsclmaand[cl][weekend]

def pickDateTime(cl='Brand: Gebouwbrand', weekend=0):
    dates = pd.date_range('2017-1-1', periods=365, freq='D')
    list= []
    for i in range(n):
        month = np.random.choice(range(1,13), 1, p=monthpdf(cl=cl ,weekend=weekend))[0]
        day = random.choice(dates[dates.month==month].day)
        list.append(
        pd.Timestamp('2017-' + str(month) +'-' + str(day)) 
        + pd.Timedelta(hours=np.random.choice(range(24), 1, p=hourspdf(cl=cl, weekend=weekend))[0])
        + pd.Timedelta(minutes=random.choice(range(60))))
    return list



d = gen_inc_cl()
for i in d:
    d['datetime'] = pickDateTime(n=len(d))
    


    
"""    
def firefury(baseline = 1000,nyeardays=365):
    if random.random() <0.5:
        n = baseline + int(random.random()*100)
    else:
        n = baseline - int(random.random()*100)
    print 'n= ' + str(n)
    fires = bag.sample(n)
    fires = fires.reset_index()[['pandid','objectid','rds_x','rds_y']]
    fires['datetime'] = pickDateTime(len(fires))
    fires['hour'] = fires['datetime'].dt.hour
    return fires

    

fires = firefury(baseline = 1000)
fires.to_csv('fires.csv',sep=';')

"""





